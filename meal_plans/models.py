from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    date = models.DateField(auto_now_add=False)
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
    )

    def __str__(self):
        return self.name
